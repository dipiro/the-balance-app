//
//  CurrencyData.swift
//  BalanceApp
//
//  Created by piro on 22.03.21.
//

import Foundation

struct CurrencyData: Codable {
    let data: [Currency]?
}

struct Currency: Codable {
    let id: String?
    let rateUsd: String?
}

