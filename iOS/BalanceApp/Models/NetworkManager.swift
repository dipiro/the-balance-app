//
//  NetworkManager.swift
//  BalanceApp
//
//  Created by piro on 22.03.21.
//

import Foundation
import RealmSwift

// https://free.currconv.com/api/v7/convert?q=USD_BYN,EUR_BYN&compact=ultra&apiKey=ce239d68182707d967c8

// https://api.coincap.io/v2/assets/bitcoin

struct NetworkManager {
    
    static func fetchCurrency(completion: @escaping(CurrencyModel) ->()) {
        
        guard let url = URL(string: "https://api.coincap.io/v2/rates") else { return }
        URLSession.shared.dataTask(with: url) { (Data, Response, Error) in
            DispatchQueue.global(qos: .background).async {
                if let response = Response {
                    print(response)
                }
                if let error = Error {
                    print(error)
                }
                if let data = Data {
                    DispatchQueue.global(qos: .utility).async {
                        do {
                            let cryptocurrencyData = try JSONDecoder().decode(CurrencyData.self, from: data)
                            
                            guard let cryptData =  cryptocurrencyData.data else { return }
                            
                            var array: [Currency] = []
                            for cryptocurrency in cryptData where cryptocurrency.id == "bitcoin" {
                                array.append(cryptocurrency)
                            }
                            for cryptocurrency in cryptData where cryptocurrency.id == "dogecoin" {
                                array.append(cryptocurrency)
                            }
                            for cryptocurrency in cryptData where cryptocurrency.id == "belarusian-ruble" {
                                array.append(cryptocurrency)
                            }
                            for cryptocurrency in cryptData where cryptocurrency.id == "euro" {
                                array.append(cryptocurrency)
                            }
                            
                            let arrayModel = CurrencyModel(model: array)
                            completion(arrayModel)
                            
                        } catch let error {
                            print(error.localizedDescription)
                        }
                    }
                }
            }
        }.resume()
    }
    
}
