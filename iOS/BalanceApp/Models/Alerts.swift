//
//  CustomAlert.swift
//  BalanceApp
//
//  Created by piro on 19.04.21.
//

import Foundation
import UIKit

class Alerts {
    
    static func alertForSubcategory(parentViewController: UIViewController, model: AnyObject) {
        let alert = ForSubcategory(nibName: ForSubcategory.identifier, bundle: nil)
        alert.modalPresentationStyle = .custom
        alert.modalTransitionStyle = .crossDissolve
        alert.object = model
        alert.delegate = parentViewController as! AlertDelegate
        parentViewController.present(alert, animated: true, completion: nil)
    }
    
    static func AlertMain(parentViewController: UIViewController) {
        let alert = MainAlert(nibName: MainAlert.identifier, bundle: nil)
        alert.modalPresentationStyle = .formSheet
        alert.delegate = parentViewController as! AlertDelegate
        parentViewController.present(alert, animated: true, completion: nil)
    }
}
