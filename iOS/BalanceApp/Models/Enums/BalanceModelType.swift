//
//  BalanceModelType.swift
//  BalanceApp
//
//  Created by piro on 21.03.21.
//

import Foundation

enum BalanceModelType: Int {
    case profit = 0
    case cost
    
    static let count: Int = {
        var max: Int = 0
        while let _ = BalanceModelType(rawValue: max) { max += 1 }
        return max
    }()
}
