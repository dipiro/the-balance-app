//
//  CurrencyType.swift
//  BalanceApp
//
//  Created by piro on 25.03.21.
//

import Foundation

enum CurrencyType {
    case eur
    case usd
    case byn
}
