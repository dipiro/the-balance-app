//
//  CellType.swift
//  BalanceApp
//
//  Created by piro on 22.03.21.
//

import Foundation

enum CellType: Int {
    case chartsType = 0
    case currencyType
    case customType
    
    static let count: Int = {
        var max: Int = 0
        while let _ = CellType(rawValue: max) { max += 1 }
        return max
    }()
}
