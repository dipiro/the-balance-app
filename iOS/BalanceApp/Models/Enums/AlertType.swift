//
//  AlertType.swift
//  BalanceApp
//
//  Created by piro on 22.03.21.
//

import Foundation

enum AlertType {
    case addNewCellProfit
    case addNewCellCost
    case addNewSubCell
}
