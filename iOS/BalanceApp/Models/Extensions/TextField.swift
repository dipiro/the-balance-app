//
//  TextField.swift
//  BalanceApp
//
//  Created by piro on 19.04.21.
//

import Foundation
import UIKit

extension UITextField {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
         self.view.endEditing(true)
         return false
     }
}
