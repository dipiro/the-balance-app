//
//  UIView.swift
//  BalanceApp
//
//  Created by piro on 14.03.21.
//

import Foundation
import UIKit

extension UIView {
    
    class func loadFromNib<T: UIView>() -> T {
        return Bundle.main.loadNibNamed(String(describing: T.self), owner: nil, options: nil)![0] as! T
    }
    
    func roundedView() {
        self.layer.cornerRadius = 12
    }
}
