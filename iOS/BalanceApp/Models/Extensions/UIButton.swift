//
//  UIButton.swift
//  BalanceApp
//
//  Created by piro on 2.04.21.
//

import Foundation
import UIKit

extension UIButton {
    
    func roundedButton() {
        self.layer.cornerRadius = 12
    }
    
    func roundButton() {
        self.layer.cornerRadius = 0.5 * self.bounds.size.width
    }
}
