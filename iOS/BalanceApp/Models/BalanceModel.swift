//
//  BalanceModel.swift
//  BalanceApp
//
//  Created by piro on 11.03.21.
//

import Foundation
import RealmSwift
import UIKit

class Revenue: Object {
    @objc dynamic var name: String = ""
    @objc dynamic var image: String = ""
    @objc dynamic var amount: Double {
        get {
            return subcategory.map({$0.amount}).reduce(0, +)
        }
    }
    var subcategory = List<Subcategory>()
    
    convenience init(name: String, image: String) {
        self.init()
        self.name = name
        self.image = image
    }
    
}

class Expense: Object {
    @objc dynamic var name: String = ""
    @objc dynamic var image: String = ""
    @objc dynamic var amount: Double {
        get {
            return subcategory.map({$0.amount}).reduce(0, +)
        }
    }
    var subcategory = List<Subcategory>()
    
    convenience init(name: String, image: String) {
        self.init()
        self.name = name
        self.image = image
    }
}

class Subcategory: Object {
    @objc dynamic var name: String = ""
    @objc dynamic var amount: Double = 0.0
    
    convenience init(name: String, amount: Double) {
        self.init()
        self.name = name
        self.amount = amount
    }
}
