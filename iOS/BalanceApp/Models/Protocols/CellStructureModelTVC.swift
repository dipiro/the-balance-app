//
//  TVCViewModelType.swift
//  BalanceApp
//
//  Created by piro on 17.03.21.
//

import Foundation

protocol CellStructureModelTVC: class {
    var name: String { get }
    var cost: String { get }
    var image: String { get }
}
 
