//
//  TVViewModelType.swift
//  BalanceApp
//
//  Created by piro on 17.03.21.
//

import Foundation
import RealmSwift

protocol FirstViewModalProtocol {
    
    func costNumberOfRows() -> Int
    
    func profitNumberOfRows() -> Int
    
    func cellViewModel(forIndexPath indexPath: IndexPath, type: BalanceModelType) -> CellStructureModelTVC?
        
    func viewModelForSelectedRow(forIndexPath indexPath: IndexPath, type: BalanceModelType) -> DetailViewModelType?
    
    func getModel(indexPath: IndexPath)  -> AnyObject
    
    func saveModel(typeModel: BalanceModelType, nameCell: String, image: String)
    
    func fetchUpdate()
    
    func getInCurrency(byn: Double, currencyType: CurrencyType) -> Double?

    func getImageForCell(indexPath: IndexPath) -> String
    
    func getImagesCount() -> Int
    
    func getImageForCollectionView(indexPath: IndexPath) -> UIImage
    
    func getDifference() -> String
    
    func cellViewModelForDeleting(forIndexPath indexPath: IndexPath, type: BalanceModelType) -> AnyObject


}
