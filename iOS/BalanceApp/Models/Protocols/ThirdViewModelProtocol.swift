//
//  ThirdViewModelProtocol.swift
//  BalanceApp
//
//  Created by piro on 31.03.21.
//

import Foundation
import UIKit

protocol ThirdViewModelProtocol {
    
    func getCountOfModels() -> Int
    func getCostOfModels(element: Int) -> String 
    func getNameOfModels(element: Int) -> String
    func colorsOfCharts() -> UIColor
}
