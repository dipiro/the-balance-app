//
//  DetailViewModelType.swift
//  BalanceApp
//
//  Created by piro on 19.03.21.
//

import Foundation
import RealmSwift

protocol DetailViewModelType {
    func numberOfRows() -> Int
    func cellViewModel(forIndexPath indexPath: IndexPath) -> CellStructureModelTVC?
}
