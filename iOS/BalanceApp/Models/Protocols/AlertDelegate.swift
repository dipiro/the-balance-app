//
//  AlertDelegate.swift
//  BalanceApp
//
//  Created by piro on 19.04.21.
//

import Foundation

protocol AlertDelegate: class {
    func tableViewReload()
}
