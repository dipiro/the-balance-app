//
//  InfoViewModelProtocol.swift
//  BalanceApp
//
//  Created by piro on 26.03.21.
//

import Foundation
import UIKit

protocol SecondViewModelProtocol {

    func getCostOfCurrency(indexPath: IndexPath) -> String
    func setImages(indexPath: IndexPath) -> UIImage
    func getCountOfImages() -> Int
    func getNameOfModel() -> [String]
    func getExpenseOfModel() -> [Double]
}
