//
//  CurrencyModel.swift
//  BalanceApp
//
//  Created by piro on 22.03.21.
//

import Foundation
import RealmSwift
import UIKit

class CurrencyModel: Object {
    @objc dynamic var bitcoinToUsd: Double = 0.0
    @objc dynamic var dogecoinToUsd: Double = 0.0
    @objc dynamic var euroToUsd: Double = 0.0
    @objc dynamic var bynToUsd: Double = 0.0
    
    convenience init(model: [Currency]) {
        self.init()
        guard let rateUsdZero = model[0].rateUsd,
              let rateUsdOne = model[1].rateUsd,
              let rateUsdTwo = model[2].rateUsd,
              let rateUsdThree = model[3].rateUsd else { return }
        self.bitcoinToUsd = Double(rateUsdZero)!
        self.dogecoinToUsd = Double(rateUsdOne)!
        self.bynToUsd = Double(rateUsdTwo)!
        self.euroToUsd = Double(rateUsdThree)!

    }
}

//class BitocinToUSD: Object {
//    @objc dynamic var rate: Double
//    @objc dynamic var dogecoinUsd: UIImage
//
//    override init() {
//        <#code#>
//    }
//}
//
//class DogeCoinToUSD: Object {
//    <#code#>
//}
//
//class EuroToUSD: Object {
//    <#code#>
//}
//
//class BynToUSD: Object {
//    <#code#>
//}

//class Currency<T>: Object {
//    @objc dynamic var rate: Double
//    @objc dynamic var image: String
//    
//    init(rate: Double, image: String) {
//        self.rate = rate
//        self.image = image
//    }
//}
