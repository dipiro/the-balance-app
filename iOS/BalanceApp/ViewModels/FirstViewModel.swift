//
//  viewModel.swift
//  BalanceApp
//
//  Created by piro on 17.03.21.
//

import Foundation
import RealmSwift

class FirstViewModel: FirstViewModalProtocol {
    
    private var cost: Results<Expense> = {realm.objects(Expense.self)}()
    private var profit: Results<Revenue> = {realm.objects(Revenue.self)}()
    private var currency: Results<CurrencyModel> = {realm.objects(CurrencyModel.self)}()
    private var currentObject: AnyObject?
    private let arrayImagesString = ["beer", "percent", "restaurant", "wallet", "bag", "car", "home", "smoking"]
    
    func getDifference() -> String {
        let sumCost = cost.map({$0.amount}).reduce(0, +)
        let sumProfit = profit.map({$0.amount}).reduce(0, +)
        let difference =  String(format: "%.2f", sumProfit - sumCost) + " BYN"
        
        return difference
    }
    
    func costNumberOfRows() -> Int {
        cost.count
    }
    
    func profitNumberOfRows() -> Int {
        profit.count 
    }
    
    func cellViewModel(forIndexPath indexPath: IndexPath, type: BalanceModelType) -> CellStructureModelTVC? {
        switch type {
        case .profit:
            return CellStructureTVC(model: profit[indexPath.row])
        case .cost:
            return CellStructureTVC(model: cost[indexPath.row])
        }
    }
    
    func viewModelForSelectedRow(forIndexPath indexPath: IndexPath, type: BalanceModelType) -> DetailViewModelType? {
        switch type {
        case .profit:
            return DetailViewModel(category: profit[indexPath.row].subcategory)
        case .cost:
            return DetailViewModel(category: cost[indexPath.row].subcategory)
        }
    }
    
    func getModel(indexPath: IndexPath) -> AnyObject {
        if indexPath.section == 0 {
            self.currentObject = profit[indexPath.row]
            return profit[indexPath.row]
        } else {
            self.currentObject = cost[indexPath.row]
            return cost[indexPath.row]
        }
    }
    
    func saveModel(typeModel: BalanceModelType, nameCell: String, image: String) {
        switch typeModel {
        case .profit:
            let profit = Revenue(name: nameCell, image: image)
            StorageManager.saveObject(profit)
        case .cost:
            let cost = Expense(name: nameCell, image: image)
            StorageManager.saveObject(cost)
        }
        
    }
    
    func fetchUpdate() {

        NetworkManager.fetchCurrency { [weak self] (cryptocurrencyData) in
            DispatchQueue.main.async {
                if self?.currency.count == 0 {
                    StorageManager.saveObject(cryptocurrencyData)
                } else {
                    try! realm.write() {
                        self?.currency.first?.bitcoinToUsd = cryptocurrencyData.bitcoinToUsd
                        self?.currency.first?.dogecoinToUsd = cryptocurrencyData.dogecoinToUsd
                        self?.currency.first?.euroToUsd = cryptocurrencyData.euroToUsd
                        self?.currency.first?.bynToUsd = cryptocurrencyData.bynToUsd
                    }
                }
            }
        }
    }
    
    func getInCurrency(byn: Double, currencyType: CurrencyType) -> Double? {
        guard let currency = currency.first else { return nil }
        
        switch currencyType {
        case .usd:
            return byn / currency.bynToUsd
        case .eur:
            return (byn / currency.bynToUsd) * currency.euroToUsd
        case .byn:
            return byn
        }
    }
    
    func getImageForCell(indexPath: IndexPath) -> String {
        return arrayImagesString[indexPath.row]
    }
    
    func getImagesCount() -> Int {
        arrayImagesString.count
    }
    
    func getImageForCollectionView(indexPath: IndexPath) -> UIImage {
        return UIImage(named: arrayImagesString[indexPath.row])!
    }
    
    func cellViewModelForDeleting(forIndexPath indexPath: IndexPath, type: BalanceModelType) -> AnyObject {
        switch type {
        case .profit:
            return profit[indexPath.row]
        case .cost:
            return cost[indexPath.row]
        }
    }
    
}

