//
//  ThirdViewModel.swift
//  BalanceApp
//
//  Created by piro on 31.03.21.
//

import Foundation
import RealmSwift

class ThirdViewModel: ThirdViewModelProtocol {
    
    private var expenses: Results<Expense> = {realm.objects(Expense.self)}()
    
    var sortedArrayOfExpenses: [[String]] {
        get {
            let sortedArray = expenses
                .map { index in [index.name, String(format: "%.2f", index.amount)] }
                .sorted { $0[1] < $1[1] }
            
            return sortedArray
        }
    }
    
    func getCountOfModels() -> Int {
        return sortedArrayOfExpenses.count
    }
    
    func getNameOfModels(element: Int) -> String {
        let names = sortedArrayOfExpenses.map{ $0[0] }
        return names[element]
        
    }
    
    func getCostOfModels(element: Int) -> String {
        let expenses = sortedArrayOfExpenses.map{ $0[1] }
        return expenses[element]
    }
    
    
    func colorsOfCharts() -> UIColor {
        let red = Double(arc4random_uniform(256))
        let green = Double(arc4random_uniform(256))
        let blue = Double(arc4random_uniform(256))
        let color = UIColor(red: CGFloat(red/255), green: CGFloat(green/255), blue: CGFloat(blue/255), alpha: 1)
        return color
    }
}
