//
//  InfoViewModel.swift
//  BalanceApp
//
//  Created by piro on 26.03.21.
//

import Foundation
import RealmSwift

class SecondViewModel: SecondViewModelProtocol {
    
    private var arrayOfImages = [UIImage(named: "rub"),
                                 UIImage(named: "usd"),
                                 UIImage(named: "eur"),
                                 UIImage(named: "bitcoin"),
                                 UIImage(named: "dogecoin")]
    
    private var expenses: Results<Expense> = {realm.objects(Expense.self)}()
    private var currency: Results<CurrencyModel> = {realm.objects(CurrencyModel.self)}()
    
    func setImages(indexPath: IndexPath) -> UIImage {
        return arrayOfImages[indexPath.row]!
    }
    
    func getCostOfCurrency(indexPath: IndexPath) -> String {
        guard let cryptocurrency = currency.first else { return "error" }
        
        var arrayOfCost: [String] = []
        
        let byn = expenses.map({$0.amount}).reduce(0, +)
        
        let dollarsDouble = byn * cryptocurrency.bynToUsd
        
        let dollarsString = String(format: "%.2f", dollarsDouble)
        let bynString = String(format: "%.2f", byn)
        let dogeCoin = String(format: "%.3f", dollarsDouble / cryptocurrency.dogecoinToUsd)
        let bytcoin = String(format: "%.3f", dollarsDouble / cryptocurrency.bitcoinToUsd)
        let euro = String(format: "%.2f", dollarsDouble / cryptocurrency.euroToUsd)

        arrayOfCost.append(contentsOf: [bynString, dollarsString, euro, bytcoin, dogeCoin])
        
        return arrayOfCost[indexPath.row]
        
    }
    
    func getCountOfImages() -> Int {
        return arrayOfImages.count
    }
    
    func getNameOfModel() -> [String] {
        return expenses.map { $0.name }
    }
    
    func getExpenseOfModel() -> [Double] {
        return expenses.map { $0.amount }
    }
    
    
}
