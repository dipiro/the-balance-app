//
//  TVCViewModel.swift
//  Pods
//
//  Created by piro on 17.03.21.
//

import Foundation

class CellStructureTVC: CellStructureModelTVC {

    private var model: AnyObject
    
    var name: String {
        return model.name
    }
    
    var image: String {
        return model.image!
    }
    
    var cost: String {
        return String(format: "%.2f", model.amount)
    }
    
    init(model: AnyObject) {
        self.model = model
    }
    
}
