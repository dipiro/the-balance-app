//
//  DetailViewModel.swift
//  BalanceApp
//
//  Created by piro on 19.03.21.
//

import Foundation
import RealmSwift

class DetailViewModel: DetailViewModelType {
    
   private var category: List<Subcategory>?
    
    func numberOfRows() -> Int {
        guard let categoryCount = category?.count else { return 0 }
        
        return categoryCount
    }
    
    func cellViewModel(forIndexPath indexPath: IndexPath) -> CellStructureModelTVC? {
        let subcategory = category![indexPath.row]
        return CellStructureTVC(model: subcategory)
    }
    
    init(category: List<Subcategory>) {
        self.category = category
    }
    
}
