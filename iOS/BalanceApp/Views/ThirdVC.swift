//
//  ArVc.swift
//  BalanceApp
//
//  Created by piro on 30.03.21.
//

import Foundation
import SceneKit
import ARKit

class ThirdVC: UIViewController {
    
    @IBOutlet var ARView: ARSCNView!
    
    var viewModel: ThirdViewModelProtocol = ThirdViewModel()
    var planes = [Plane]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        ARView.delegate = self
        
        ARView.autoenablesDefaultLighting = true
        
        let scene = SCNScene()
        
        ARView.scene = scene
        
        setupGesture()
    }
    
    
    func setupGesture() {
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(placeBox))
        tapGestureRecognizer.numberOfTapsRequired = 1
        self.ARView.addGestureRecognizer(tapGestureRecognizer)
    }
    
    @objc func placeBox(tapGesture: UITapGestureRecognizer) {
        
        let sceneView = tapGesture.view as! ARSCNView
        let location = tapGesture.location(in: sceneView)
        
        let hitTestResult = sceneView.raycastQuery(from: location, allowing: .existingPlaneGeometry, alignment: .any)
        guard let hitResult = sceneView.session.raycast(hitTestResult!).first else { return }
        
        createFigures(hitResult: hitResult )
        
    }
    
    
    private func createFigures(hitResult: ARRaycastResult) {
        var height: CGFloat = 0.1
        var xCoordinate: Float = 0
        ARView.autoenablesDefaultLighting = true
        
        for element in 0..<viewModel.getCountOfModels() {
            
            //object
            let boxGeometry = SCNBox(width: 0.1, height: height, length: 0.1, chamferRadius:  0.01)
            
            //color
            let material = SCNMaterial()
            material.diffuse.contents = viewModel.colorsOfCharts()
            
            //nameText
            let nameTextGeometry = SCNText(string: viewModel.getNameOfModels(element: element), extrusionDepth: 2.0)
            let nameTextMaterial = SCNMaterial()
            nameTextMaterial.diffuse.contents = UIColor.white
            
            //costText
            let costTextGeometry = SCNText(string: viewModel.getCostOfModels(element: element), extrusionDepth: 2.0)
            let costTextMaterial = SCNMaterial()
            costTextMaterial.diffuse.contents = UIColor.white
            
            //boxNode
            let boxNode = SCNNode(geometry: boxGeometry)
            boxNode.geometry?.materials = [material]
            //                boxNode.position = SCNVector3(xCoordinate, 0, -1)
            boxNode.position = SCNVector3( hitResult.worldTransform.columns.3.x + xCoordinate,
                                           hitResult.worldTransform.columns.3.y + Float(height / 2),
                                           hitResult.worldTransform.columns.3.z)
            
            //nameTextNode
            let nameTextNode = SCNNode(geometry: nameTextGeometry)
            nameTextNode.scale = SCNVector3(0.002, 0.002, 0.002)
            nameTextNode.geometry?.materials = [nameTextMaterial]
            //                textNode.position = SCNVector3(xCoordinate, 0.2, -1.0)
            nameTextNode.position = SCNVector3( hitResult.worldTransform.columns.3.x - 0.05 + xCoordinate,
                                                hitResult.worldTransform.columns.3.y + Float(height + 0.025),
                                                hitResult.worldTransform.columns.3.z)
            
            //costTextNode
            let costTextNode = SCNNode(geometry: costTextGeometry)
            costTextNode.scale = SCNVector3(0.0025, 0.0025, 0.0025)
            costTextNode.geometry?.materials = [costTextMaterial]
            //                textNode.position = SCNVector3(xCoordinate, 0.2, -1.0)
            costTextNode.position = SCNVector3( hitResult.worldTransform.columns.3.x - 0.05 + xCoordinate,
                                            hitResult.worldTransform.columns.3.y,
                                            hitResult.worldTransform.columns.3.z + 0.15)
            height += 0.05
            xCoordinate += 0.125
            
            ARView.scene.rootNode.addChildNode(boxNode)
            ARView.scene.rootNode.addChildNode(nameTextNode)
            ARView.scene.rootNode.addChildNode(costTextNode)
        }
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Create a session configuration
        let configuration = ARWorldTrackingConfiguration()
        
        configuration.planeDetection = .horizontal
        
        // Run the view's session
        ARView.session.run(configuration)
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Pause the view's session
        ARView.session.pause()
//        ARView.removeFromSuperview()
//        sceneView = nil
        
        
    }
}

extension ThirdVC: ARSCNViewDelegate {
    
    func renderer(_ renderer: SCNSceneRenderer, didAdd node: SCNNode, for anchor: ARAnchor) {
        
        guard anchor is ARPlaneAnchor else { return }
        
        let plane = Plane(anchor: anchor as! ARPlaneAnchor)
        
        self.planes.append(plane)
        node.addChildNode(plane)
    }
    
    func renderer(_ renderer: SCNSceneRenderer, didUpdate node: SCNNode, for anchor: ARAnchor) {
        
        let plane = self.planes.filter { plane in
            return plane.anchor.identifier == anchor.identifier
        }.first
        
        guard plane != nil else { return }
        
        plane?.update(anchor: anchor as! ARPlaneAnchor )
    }
}
