//
//  InfoVC.swift
//  BalanceApp
//
//  Created by piro on 22.03.21.
//

import Foundation
import Charts
import RealmSwift


class SecondVC: UIViewController  {
    
// MARK: -Outlets
    @IBOutlet weak var chartView: PieChartView!
    @IBOutlet weak var collectionView: UICollectionView! {
        didSet {
            collectionView.register(UINib(nibName: CurrencyCVC.identifier, bundle: nil), forCellWithReuseIdentifier: CurrencyCVC.identifier)
            collectionView.delegate = self
            collectionView.dataSource = self
        }
    }
// MARK: -Variables
    private var barChart = BarChartView()
    private var viewModel: SecondViewModelProtocol = SecondViewModel()
    
// MARK: -Functions
    override func viewDidLoad() {
        super.viewDidLoad()
                
        self.navigationController?.navigationBar.prefersLargeTitles = true
        navigationItem.title = "You have spent"
    
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        customizeChart(dataPoints: viewModel.getNameOfModel(), values: viewModel.getExpenseOfModel())
        
        collectionView.reloadData()
    }
    
    func customizeChart(dataPoints: [String], values: [Double]) {
        // 1. Set ChartDataEntry
        var dataEntries: [ChartDataEntry] = []
        for element in 0..<dataPoints.count {
            let dataEntry = PieChartDataEntry(value: values[element], label: dataPoints[element], data: dataPoints[element] as AnyObject)
            dataEntries.append(dataEntry)
        }
        // 2. Set ChartDataSet
        let pieChartDataSet = PieChartDataSet(entries: dataEntries, label: nil)
        pieChartDataSet.colors = colorsOfCharts(numbersOfColor: dataPoints.count)
        // 3. Set ChartData
        let pieChartData = PieChartData(dataSet: pieChartDataSet)
        let format = NumberFormatter()
        format.numberStyle = .none
        let formatter = DefaultValueFormatter(formatter: format)
        pieChartData.setValueFormatter(formatter)
        // 4. Assign it to the chart’s data
        chartView.data = pieChartData
    }
    
    private func colorsOfCharts(numbersOfColor: Int) -> [UIColor] {
        var colors: [UIColor] = []
        for _ in 0..<numbersOfColor {
            let red = Double(arc4random_uniform(256))
            let green = Double(arc4random_uniform(256))
            let blue = Double(arc4random_uniform(256))
            let color = UIColor(red: CGFloat(red/255), green: CGFloat(green/255), blue: CGFloat(blue/255), alpha: 1)
            colors.append(color)
        }
        return colors
    }

}

// MARK: - CollectionView
extension SecondVC: UICollectionViewDelegate, UICollectionViewDataSource  {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.getCountOfImages()
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CurrencyCVC.identifier, for: indexPath) as? CurrencyCVC else { return UICollectionViewCell() }
        
        cell.imageView.image = viewModel.setImages(indexPath: indexPath)
        cell.currencyLabel.text = viewModel.getCostOfCurrency(indexPath: indexPath)

        return cell
    }
    
}
