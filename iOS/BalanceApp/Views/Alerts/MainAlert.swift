//
//  AlertVC.swift
//  BalanceApp
//
//  Created by piro on 22.03.21.
//

import UIKit

class MainAlert: UIViewController {

    static let identifier = "MainAlert"
    
    @IBOutlet weak var collectionViewBackground: UIView! {
        didSet {
            collectionViewBackground.roundedView()
        }
    }
    @IBOutlet weak var collectionView: UICollectionView! {
        didSet {
            collectionView.register(UINib(nibName: "CustomCVC", bundle: nil), forCellWithReuseIdentifier: CustomCVC.identifier)
            collectionView.dataSource = self
            collectionView.delegate = self
        }
    }
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var choiceSegmentedControl: UISegmentedControl!
    @IBOutlet weak var cellNamesTextField: UITextField! {
        didSet {
            cellNamesTextField.returnKeyType = UIReturnKeyType.done
            cellNamesTextField.delegate = self
        }
    }
    @IBOutlet weak var saveButton: UIButton! {
        didSet {
            saveButton.roundedButton()
        }
    }
    
    var isExpand: Bool = false
    var delegate: AlertDelegate?
    var viewModel: FirstViewModalProtocol = FirstViewModel()
    
    @IBAction func tappedButton(_ sender: UIButton) {
        guard let text = cellNamesTextField.text,
              !text.isEmpty,
              let indexPath = collectionView.indexPathsForSelectedItems?.first else { return }
        
        switch choiceSegmentedControl.selectedSegmentIndex {
        case 0:
            viewModel.saveModel(typeModel: .profit, nameCell: text, image: viewModel.getImageForCell(indexPath: indexPath))
        case 1:
            viewModel.saveModel(typeModel: .cost, nameCell: text, image: viewModel.getImageForCell(indexPath: indexPath))
        default:
            viewModel.saveModel(typeModel: .profit, nameCell: text, image: viewModel.getImageForCell(indexPath: indexPath))
        }
        
        delegate?.tableViewReload()
        dismiss(animated: true, completion: nil)
    }
}
 
extension MainAlert: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.getImagesCount()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 50, height: 50)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CustomCVC.identifier, for: indexPath) as? CustomCVC else { return UICollectionViewCell() }
        
        cell.ImageV.image = viewModel.getImageForCollectionView(indexPath: indexPath)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CustomCVC.identifier, for: indexPath) as? CustomCVC else { return }
        
        cell.ImageV.backgroundColor = .red
        cell.tintColor = .green
        cell.backgroundColor = .blue
    }
}

extension MainAlert: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
         self.view.endEditing(true)
         return false
     }
}
