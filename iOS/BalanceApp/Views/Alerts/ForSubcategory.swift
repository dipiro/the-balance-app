//
//  ForCellAlert.swift
//  BalanceApp
//
//  Created by piro on 22.03.21.
//

import UIKit

class ForSubcategory: UIViewController {

// MARK: -Outlets
    static let identifier = "ForSubcategory"
    
    @IBOutlet weak var background: UIView! {
        didSet {
            background.roundedView()
        }
    }
    @IBOutlet weak var currencyPickerView: UIPickerView! {
        didSet {
            currencyPickerView.delegate = self
            currencyPickerView.dataSource = self
        }
    }
    @IBOutlet var buttons: [UIButton]! {
        didSet {
            buttons.map({$0.roundedButton()})
        }
    }
    
    @IBOutlet weak var nameCellLabel: UILabel!
    @IBOutlet weak var costTextField: UITextField! {
        didSet {
            costTextField.returnKeyType = UIReturnKeyType.done
            costTextField.delegate = self
        }
    }
    @IBOutlet weak var nameTextField: UITextField! {
        didSet {
            nameTextField.returnKeyType = UIReturnKeyType.done
            nameTextField.delegate = self
        }
    }
    
// MARK: -Variables
    var viewModel: FirstViewModalProtocol = FirstViewModel()
    var array = ["BYN", "USD", "EUR"]
    var object: AnyObject?
    var delegate: AlertDelegate?
    
    var onAction: (()->())?

// MARK: -Functions
    
    override func viewDidLoad() {
        super.viewDidLoad()
        nameCellLabel.text = object?.name
    }
    
    @IBAction func pressedCloseButton(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func pressedSaveButton(_ sender: UIButton) {
        guard let textCost = costTextField.text,
              !textCost.isEmpty,
              let textCostDouble = Double(textCost),
              let textName = nameTextField.text,
              !textName.isEmpty else { return }
                
        var cost: Double
        let pickerRow = self.currencyPickerView.selectedRow(inComponent: 0)
        switch pickerRow {
        case 0:
            cost = viewModel.getInCurrency(byn: textCostDouble, currencyType: .byn) ?? textCostDouble
        case 1:
            cost = viewModel.getInCurrency(byn: textCostDouble, currencyType: .usd) ?? 0.0
        case 2:
            cost = viewModel.getInCurrency(byn: textCostDouble, currencyType: .eur) ?? 0.0
        default:
            cost = viewModel.getInCurrency(byn: textCostDouble, currencyType: .byn) ?? 0.0
        }
        
        if object is Expense {
            let model = realm.objects(Expense.self).filter({ $0.name == "\(self.object!.name!)" })
            if let modelFirst = model.first {
                try! realm.write {
                    let costCost = Subcategory(name: textName, amount: cost)
                                        modelFirst.subcategory.insert(costCost, at: 0)
                }
            }
        } else {
            let model = realm.objects(Revenue.self).filter({ $0.name == "\(self.object!.name!)" })
            if let modelFirst = model.first {
                try! realm.write {
                    let costCost = Subcategory(name: textName, amount: cost)
                                        modelFirst.subcategory.insert(costCost, at: 0)
                }
            }
        }
        
        costTextField.text = nil
        nameTextField.text = nil
        
        delegate?.tableViewReload()
        
        dismiss(animated: true, completion: nil)
        

    }
}

// MARK: - PickerView
extension ForSubcategory: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        array.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return array[row]
    }
    
}

extension ForSubcategory: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
         self.view.endEditing(true)
         return false
     }
}
