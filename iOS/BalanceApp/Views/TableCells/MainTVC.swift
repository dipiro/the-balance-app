//
//  CustomTVC.swift
//  BalanceApp
//
//  Created by piro on 4.03.21.
//

import UIKit

class MainTVC: UITableViewCell {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var costLabel: UILabel!
    @IBOutlet weak var imageImageView: UIImageView!
    
    static let identificator = "MainTVC"
    
    weak var viewModel: CellStructureModelTVC? {
        willSet(viewModel) {
            DispatchQueue.main.async {
                guard let viewModel = viewModel else { return }
                self.nameLabel.text = viewModel.name
                self.costLabel.text = viewModel.cost
                self.imageImageView.image = UIImage(named: viewModel.image)
            }
        }
    }
    
    weak var subViewModel: CellStructureModelTVC? {
        willSet(subViewModel) {
            DispatchQueue.main.async {
                guard let subViewModel = subViewModel else { return }
                self.nameLabel.text = subViewModel.name
                self.costLabel.text = subViewModel.cost
            }
        }
    }
    
    
}
