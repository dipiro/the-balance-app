//
//  ViewController.swift
//  BalanceApp
//
//  Created by piro on 4.03.21.
//

import UIKit
import RealmSwift

class FirstVC: UIViewController {
    
// MARK: -Outlets
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            tableView.register(UINib(nibName: MainTVC.identificator, bundle: nil), forCellReuseIdentifier: MainTVC.identificator)
            tableView.delegate = self
            tableView.dataSource = self
        }
    }
    @IBOutlet weak var addNewCellButton: UIButton! {
        didSet {
            addNewCellButton.roundButton()
        }
    }
    
// MARK: -Variables
    private var viewModel: FirstViewModalProtocol?
    
// MARK: -Functions
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.overrideUserInterfaceStyle = .light
        
        self.navigationController?.navigationBar.prefersLargeTitles = true
        
        viewModel = FirstViewModel()
        viewModel?.fetchUpdate()
    
        tableView.reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        navigationItem.title = viewModel?.getDifference()
        tableView.reloadData()
    }
    
    @IBAction func pressedAddNewCellButton(_ sender: UIButton) {
        Alerts.AlertMain(parentViewController: self)
    }
}

// MARK: - TableView
extension FirstVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        if section == 0 && viewModel?.profitNumberOfRows() != 0 {
            return "Revenue"
        }
        if section == 1 && viewModel?.costNumberOfRows() != 0  {
            return "Expenses"
        }
        return nil
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return BalanceModelType.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let costCount = viewModel?.costNumberOfRows(),
              let profitCount = viewModel?.profitNumberOfRows(),
              let type = BalanceModelType(rawValue: section) else { return 0 }
        
        switch type {
        case .profit:
            return profitCount
        case .cost:
            return costCount
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: MainTVC.identificator, for: indexPath) as? MainTVC else { return UITableViewCell() }
        
        if indexPath.section == 0 {
            let cellViewModel = viewModel?.cellViewModel(forIndexPath: indexPath, type: .profit)
            cell.viewModel = cellViewModel
            return cell
            
            
        } else if indexPath.section == 1 {
            let cellViewModel = viewModel?.cellViewModel(forIndexPath: indexPath, type: .cost)
            cell.viewModel = cellViewModel
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let viewModel = self.viewModel else { return }
        
        let detailVC = DetailViewController(nibName: "DetailViewController", bundle: nil)
        
        if indexPath.section == 0 {
            detailVC.viewModel = viewModel.viewModelForSelectedRow(forIndexPath: indexPath, type: .profit)
            present(detailVC, animated: true, completion: nil)
            
        } else if indexPath.section == 1 {
            detailVC.viewModel = viewModel.viewModelForSelectedRow(forIndexPath: indexPath, type: .cost)
            present(detailVC, animated: true, completion: nil)
        }
    }
    
    func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let action = UIContextualAction(style: .normal,
                                        title: NSLocalizedString("Add", comment: "Add")) { [weak self] (_, _, _) in
            guard let self = self,
                  let viewModel = self.viewModel else { return }
            
//            let alertOne = UIViewController(nibName: "ForCellAlert", bundle: nil)
//            self.present(alertOne, animated: true, completion: nil)
            
            Alerts.alertForSubcategory(parentViewController: self,
                                       model: viewModel.getModel(indexPath: indexPath))

            
            
            
            
            
//            self.present(cus, animated: true, completion: nil)
            
        }
        
        action.backgroundColor = .systemPink
        let configuration = UISwipeActionsConfiguration(actions: [action])
        return configuration
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            if indexPath[0] == 0 {
                    guard let profitCell = self.viewModel?.cellViewModelForDeleting(forIndexPath: indexPath, type: .profit) else { return }
                    StorageManager.deleteObject(profitCell as! Object)
                    
            } else {
                guard let costCell = viewModel?.cellViewModelForDeleting(forIndexPath: indexPath, type: .cost) else { return }
                    StorageManager.deleteObject(costCell as! Object)
            }
        }
        navigationItem.title = viewModel?.getDifference()
        tableView.reloadData()
    }
    
}

// MARK: - AlertDelegate
extension FirstVC: AlertDelegate {
    
    func tableViewReload() {
        self.tableView.reloadData()
        navigationItem.title = viewModel?.getDifference()
    }
}
