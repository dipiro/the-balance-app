//
//  CurrencyCVC.swift
//  BalanceApp
//
//  Created by piro on 27.03.21.
//

import UIKit

class CurrencyCVC: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var currencyLabel: UILabel!
    
    static let identifier = "CurrencyCVC"

}
