//
//  CustomCVC.swift
//  BalanceApp
//
//  Created by piro on 22.03.21.
//

import UIKit

class CustomCVC: UICollectionViewCell {

    @IBOutlet weak var ImageV: UIImageView!
    
    static let identifier = "CustomCVC"
    
    override var isSelected: Bool {
        didSet {
            if self.isSelected {
                ImageV.tintColor = .systemPink
            } else {
                ImageV.tintColor = .black
            }
        }
    }
    
}
