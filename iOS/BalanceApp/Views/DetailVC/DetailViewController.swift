//
//  DetailVC.swift
//  BalanceApp
//
//  Created by piro on 17.03.21.
//

import Foundation
import UIKit

class DetailViewController: UIViewController {

    @IBOutlet weak var subTableView: UITableView! {
        didSet {
            subTableView.delegate = self
            subTableView.dataSource = self
            subTableView.register(UINib(nibName: MainTVC.identificator, bundle: nil), forCellReuseIdentifier: MainTVC.identificator)
        }
    }
    
    var viewModel: DetailViewModelType?
    
}

extension DetailViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel?.numberOfRows() ?? 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: MainTVC.identificator, for: indexPath) as? MainTVC else { return UITableViewCell() }
        
        let cellViewModel = viewModel?.cellViewModel(forIndexPath: indexPath)
        cell.subViewModel = cellViewModel

        cell.imageImageView.isHidden = true
        
        return cell
    }
    
    
}

